# Stock app :bar_chart:

- The application retrieves data from the Internet that will contain
the value of the shares of a certain company. By processing and parsing them, plotting of share values is done ​​in the corresponding time
interval.
- In addition, there is feature that calculates and displays
various statistics and an overview of the most successful companies at the moment.

## Programming Language :computer:

- C++ using Qt Library

## How to run the app :hammer:

### Requirements
- curl (to install curl run: `sudo apt install curl` and `sudo apt-get install libcurl4-openssl-dev`)
- Qt Creator and Qt library (Don't forget to install QChat)
- working internet connection

### Run from source code
- After cloning the repository to a directory of your choosing, navigate to the `source` folder. Inside, you will find `source.pro` file which you have to open with Qt Creator.
Qt Creator will automatically build the project and all that is left to do is to click on the `green Run button`.

### Note
- App has been tested only on Linux.

## Commands :keyboard:

- Use keyboard:
    - <kbd>Up</kbd> - move up on the chart view
    - <kbd>Down</kbd> - move down on the chart view
    - <kbd>Left</kbd> - move left on the chart view
    - <kbd>Right</kbd> - move right on the chart view
    - <kbd>+</kbd> - zoom in the chart view
    - <kbd>-</kbd> - zoom out the chart view
    - <kbd>Esc</kbd> - reset the chart view

- Use mouse:
    - Grab and select part of the chart view to zoom in.

## Demo Video :cinema:
- [StockApp video](https://drive.google.com/file/d/15HTYzEC9YeTWurzUddwQKskXJ8aceclM/view?usp=sharing)

## Developers :construction_worker:

- [Marko Savić 149/2019](https://gitlab.com/savicm20)
- [Mirko Kordić 242/2019](https://gitlab.com/mkordic)
- [Dragana Zdravković 309/2019](https://gitlab.com/draganaZdravkovic)
- [Marija Marković 60/2019](https://gitlab.com/marijaa010)

