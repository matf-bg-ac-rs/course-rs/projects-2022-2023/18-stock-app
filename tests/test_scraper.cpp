#include <catch2/catch.hpp>
#include "../source/scraper.hpp"

TEST_CASE("Scraper class"){

    SECTION("Get content"){
        //Arrange
        std::string url = "https://query1.finance.yahoo.com/v8/finance/chart/<ticker>?period1=<from>&period2=<to>&interval=<i>";
        const Settings s(Company::Netflix, 1669732559,1672324559, Interval::d_1);
        ParserNYSE *parser = new ParserNYSE();
        QCandlestickSeries *expected = nullptr;

        //Act
        auto output = Scraper::get_content(url,s,parser);

        //Assert
        REQUIRE_FALSE(output.second == expected);
    }

    SECTION("Get content failure") {
        std::string url = "https://query1.finance.yahoo.com/v8/finance/chart/<ticker>?period1=<from>&period2=<to>&interval=<i>";
        const Settings s(Company::Netflix, 1640649600,1672272000, Interval::m_1);
        ParserNYSE *parser = new ParserNYSE();
        QString expected = QString();

        //Act
        auto output = Scraper::get_content(url,s,parser);

        //Assert
        REQUIRE_FALSE(output.first == expected);
    }
}
