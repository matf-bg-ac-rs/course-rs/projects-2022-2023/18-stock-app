#include <catch2/catch.hpp>
#include "../source/trendingscraper.hpp"
#include<iostream>

TEST_CASE("get_content", "[function]")
{
    SECTION("Given random timestamp returns non-negative average volume value") {
//        Arrange

        ParserNYSE *parser = new ParserNYSE();
        std::string url = "https://query1.finance.yahoo.com/v8/finance/chart/PINS?period1=<from>&period2=<to>&interval=1d";

        QDateTime now = QDateTime::currentDateTime().addDays(-20);
        QDateTime a_day_ago = QDateTime::currentDateTime();
        time_t from = now.toSecsSinceEpoch();
        time_t to = a_day_ago.toSecsSinceEpoch();

        const Settings settings(Company::Netflix, from, to, Interval::d_1);

//        Act
        auto content = TrendingScraper::get_content(url, settings, parser);
//        Assert
        REQUIRE(content >= 0.0);
    }

    SECTION("Given the same day as start and end returns volume for that day and not 0.0") {
//        Arrange

        ParserNYSE *parser = new ParserNYSE();
        std::string url = "https://query1.finance.yahoo.com/v8/finance/chart/PINS?period1=<from>&period2=<to>&interval=1d";

        const Settings settings(Company::Netflix, 1669732559, 1669732559, Interval::d_1);

//        Act
        auto content = TrendingScraper::get_content(url, settings, parser);
//        Assert
        REQUIRE(content >= 0);
    }

}

TEST_CASE("find_trending_companies", "[function]")
{
    SECTION("Checking if volumes are in descending order") {
//        Arrange
        //no input values
        auto output = TrendingScraper::find_trending_companies("1d");

//        Act
        long long counter = 0;

        for(long long i  = 0; i  < output->size() - 1; i++){
            if(output->at(i).second >= output->at(i+1).second)
                counter++;
        }

//        Assert
        REQUIRE(counter == output->size() - 1);
    }

}




