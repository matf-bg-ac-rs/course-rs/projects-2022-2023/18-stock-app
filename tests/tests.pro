TEMPLATE = app
QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

QMAKE_CXXFLAGS += -g --coverage
QMAKE_LFLAGS += --coverage

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR=$$(CATCH_INCLUDE_DIR)
# set by Qt Creator wizard
isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR="./catch2"
!isEmpty(CATCH_INCLUDE_DIR): INCLUDEPATH *= $${CATCH_INCLUDE_DIR}

isEmpty(CATCH_INCLUDE_DIR): {
    message("CATCH_INCLUDE_DIR is not set, assuming Catch2 can be found automatically in your system")
}



SOURCES += \
    main.cpp \
    ../source/settings.cpp \
    ../source/parsernyse.cpp \
    ../source/scraper.cpp \
    ../source/trendingscraper.cpp \
    test_parser.cpp \
    test_scraper.cpp \
    test_settings.cpp \
    test_trendingScraper.cpp

HEADERS += \
    ../source/settings.hpp \
    ../source/parsernyse.hpp \
    ../source/scraper.hpp \
    ../source/trendingscraper.hpp

LIBS += \
    -lcurl \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
