#include <catch2/catch.hpp>
#include "../source/parsernyse.hpp"


TEST_CASE("Testing parsernyse class"){
    SECTION("Find in string"){
        //Arrange
        std::string s = "It must find word this in string";
        std::string match;
        std::regex rx("this");
        bool expected = true;
        //Act
        bool output = find_string(s, match, rx);
        //Assert
        REQUIRE(output == expected);

    }

    SECTION("Find in string 2"){
        //Arrange
        std::string s = "This test should fail";
        std::string match;
        std::regex rx("successed");
        bool expected = true;
        //Act
        bool output = find_string(s, match, rx);
        //Assert
        REQUIRE_FALSE(output == expected);
    }

    SECTION("Filter integers"){
        //Arrange
        const std::string s = "timestamps:[1,2,3,4,5]";
        const std::vector<int> expected = {1,2,3,4,5};

        //Act
        const auto output = filter_integers(s);

        //Assert
        REQUIRE(output == expected);
    }

    SECTION("Filter doubles") {
        //Arrange
        const std::string s = "open:[1.2,2.3,3.4,4.5]";
        const std::vector<double> expected = {1.2,2.3,3.4,4.5};

        //Act
        const auto output = filter_doubles(s);

        //Assert
        REQUIRE(output == expected);
    }

    SECTION("Filter integers with missing data"){
        //Arrange
        const std::string s = "timestamp:[1,2,3,null,5]";
        const std::vector<int> expected = {1,2,3,5};

        //Act
        const auto output = filter_integers(s);

        REQUIRE(output == expected);
    }

    SECTION("Filter doubles with missing data"){
        //Arrange
        const std::string s= "close:[1.2,2.3,null,4.5]";
        const std::vector<double> expected = {1.2,2.3,4.5};

        //Act
        const auto output = filter_doubles(s);

        REQUIRE(output == expected);

    }
    SECTION("Parsing failure"){
        //Arrange
        ParserNYSE *parser = new ParserNYSE();
        QCandlestickSeries *expected = nullptr;
        std::string data = "Random string that cannot be parsed";
        //Act
        auto output = parser->parse(data);

        //Assert
        REQUIRE(output.second == expected);
    }

}
