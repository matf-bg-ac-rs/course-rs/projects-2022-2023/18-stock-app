#include <catch2/catch.hpp>
#include "../source/settings.hpp"

TEST_CASE("String to company")
{
    SECTION("Given QString \"Netflix\" return Company::Netflix") {
//        Arrange
        QString input = QString("Netflix");
//        Act
        Company output = Settings::stringToCompany[input];
        Company expected = Company::Netflix;
//        Assert
        REQUIRE(output == expected);
    }

    SECTION("Test section2") { REQUIRE(0 == 0); }
}
