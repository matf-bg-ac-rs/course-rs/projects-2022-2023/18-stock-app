#ifndef EMA_HPP
#define EMA_HPP

#include <QLineSeries>
#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QCandlestickSet>
#include <QCandlestickSeries>
#include<QObjectList>
#include<deque>
#include<vector>
#include "ma.hpp"

class EMA : public StatisticsAdapter{

private:
    MA *movAvg;
    std::vector<double> indicators;
    double EMAn(QCandlestickSeries* candles,int from,int to);

public:

    EMA();

    std::vector<double> calculate(QCandlestickSeries* candles,int n);

    virtual void show(QCandlestickSeries *candles, Ui::MainWindow *ui, QChart* chart) override;
};

#endif // EMA_HPP
