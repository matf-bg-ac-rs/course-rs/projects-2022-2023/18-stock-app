#ifndef SCRAPER_HPP
#define SCRAPER_HPP

#include <QThread>
#include <QString>

#include <curl/curl.h>
#include "parsernyse.hpp"
#include "settings.hpp"

class Scraper : public QThread
{
    Q_OBJECT

public:
    //valid url for nyse: https://query1.finance.yahoo.com/v8/finance/chart/TSLA?period1=1609510767&period2=1641046767&interval=1d
    // initial url for nyse looks like: https://query1.finance.yahoo.com/v8/finance/chart/<ticker>?period1=<from>&period2=<to>&interval=<i>
   Scraper(std::string& url, Settings setting, ParserNYSE* parser, QObject *parent = nullptr);


    static QPair<QString, QCandlestickSeries*> get_content(std::string& url, const Settings& settings, const ParserNYSE* parser);


    // QThread interface
protected:
    void run() override;

    static void replace(std::string& ulr, const std::string& placeholder, const std::string& replacement);
    static void replace_placeholders(std::string& url, const Settings& settings);
    static size_t write_function(void* ptr, size_t size, size_t nmemb, std::string* data);

    std::string url;
    Settings settings;
    ParserNYSE* parser;
    QObject *parent;

signals:
    void data_is_ready(QCandlestickSeries *candles,const Settings& settings);
    void error_found(QString error);

};

#endif // SCRAPER_HPP
