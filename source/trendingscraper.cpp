#define CURL_STATICLIBS
#include "trendingscraper.hpp"

#include<string.h>
#include <iostream>

#include"parsernyse.hpp"

TrendingScraper::TrendingScraper(std::string& url, Settings settings, ParserNYSE *parser,  QObject *parent):
    Scraper(url, settings, parser, parent)
{

}

void TrendingScraper::replace_placeholders(std::string& url, const Settings& settings){

    std::string from = "<from>";
    std::string to = "<to>";

    replace(url,from, std::to_string(settings.get_time_from()));
    replace(url, to, std::to_string(settings.get_time_to()));
}

double TrendingScraper::get_content(std::string& url, Settings settings, ParserNYSE* parser){

    TrendingScraper::replace_placeholders(url, settings);
    std::string response_string;
    std::string header_string;
    auto curl = curl_easy_init();
    if(curl){

        char c_url[512];

        strcpy(c_url,url.c_str());
        curl_easy_setopt(curl, CURLOPT_URL, c_url);


        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, TrendingScraper::write_function);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_string);
        curl_easy_setopt(curl, CURLOPT_HEADERDATA, &header_string);

        auto res = curl_easy_perform(curl);
        if (res != 0){
            std::cerr << "Something is wrong with scraper" << std::endl;
            std::exit(EXIT_FAILURE);
        }

       curl_easy_cleanup(curl);
    }

    curl_global_cleanup();

    return parser->parseTrending(response_string);
}

QList<std::pair<std::string, double>>* TrendingScraper::find_trending_companies(std::string &&interval)
{
    QList<std::pair<std::string, double>>* trending_companies = new QList<std::pair<std::string, double>>();
    ParserNYSE* parser = new ParserNYSE();

    for(auto &ticker: Settings::companies_tickers){

        std::string url = "https://query1.finance.yahoo.com/v8/finance/chart/<ticker>?period1=<from>&period2=<to>&interval=" + interval;
        std::string tick = "<ticker>";
        replace(url, tick, ticker);

        QDateTime now = QDateTime::currentDateTime();
        QDateTime ten_days_ago = now.addDays(-3);
        time_t from = ten_days_ago.toSecsSinceEpoch();
        time_t to = now.toSecsSinceEpoch();

        const Settings settings(Company::Netflix, from, to, Interval::d_1);

        double volume = TrendingScraper::get_content(url, settings, parser);
        trending_companies->emplace_back(std::make_pair(ticker, volume));
    }

    std::sort(trending_companies->begin(), trending_companies->end(), [](auto a, auto b) {return a.second > b.second; });
    delete parser;
    return trending_companies;
}


void TrendingScraper::run()
{
    QList<std::pair<std::string, double>>* trending_companies = TrendingScraper::find_trending_companies("1h");
    QList<std::pair<std::string, double>>* refreshed_companies = TrendingScraper::find_trending_companies("5m");
    emit TrendingScraper::trending_ready(trending_companies);
    emit TrendingScraper::refresh_ready(refreshed_companies);
    emit TrendingScraper::data_is_ready_splash();
}
