#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QCandlestickSeries>
#include "qchart.h"
#include <QMainWindow>
#include <settings.hpp>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:

    void on_pbShowCandlestickChart_clicked();
    void init_data();
    void draw_chart(QCandlestickSeries *candles, const Settings& settings);

    void on_pbShowStatistic_clicked();
    void on_pbRefresh_clicked();
    void display_candle_values(bool status, QCandlestickSet * set);
    void show_message_box(QString error);
    void show_trending(QList<std::pair<std::string, double>>* trending_companies);
    void refresh_trending(QList<std::pair<std::string, double>>* trending_companies);
    void splash_should_close_slot();

signals:
    void candles_shown(QCandlestickSeries* candles,Ui::MainWindow *ui,QChart* chart);
    void spalsh_should_close();
private:
    Ui::MainWindow *ui;
    Settings read_settings() const;
    Settings get_init_settings() const;
};
#endif // MAINWINDOW_H
