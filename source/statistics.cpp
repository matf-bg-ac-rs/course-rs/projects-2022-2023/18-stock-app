#include "statistics.hpp"
#include "ma.hpp"
#include "ema.hpp"
#include "macd.hpp"
#include "bollinger_bands.hpp"
#include "ui_mainwindow.h"

Statistics::Statistics(){}

Statistics::Statistics(StatisticType type):
    statType(type)
{

}

bool Statistics::isThereEnoughData(QCandlestickSeries* candles){

    if(candles != nullptr){
        if(candles->count() >=20){
            return true;
        }
    }
    return false;
}

void Statistics::startDrawingStats(QCandlestickSeries* s,Ui::MainWindow *ui,QChart* chart){

    if (statType == StatisticType::MA){
        if(!isMAShown){
            MA* movAvg = new MA();
            if(isThereEnoughData(s)){
                movAvg->show(s,ui,chart);
            }
            isMAShown = true;
            delete movAvg;
        }
        else{
            return;
        }
    }
    else if(statType == StatisticType::EMA){
        if(!isEMAShown){
            EMA* expMovAvg = new EMA();
            if(isThereEnoughData(s)){
                expMovAvg->show(s,ui,chart);
            }
            isEMAShown = true;
            delete expMovAvg;
        }
        else{
            return;
        }
    }
    else if(statType == StatisticType::MACD){
        if(!isMACDShown){
            MACD* macd = new MACD();
            if(isThereEnoughData(s)){
                macd->show(s,ui,chart);
            }
            isMACDShown = true;
            delete macd;
        }
        else{
            return;
        }
    }
    else if(statType == StatisticType::BB){
        if(!isBBShown){
            BollingerBands* bb = new BollingerBands();
            if(isThereEnoughData(s)){
                bb->show(s,ui,chart);
            }
            isBBShown = true;
            delete bb;
        }
        else{
            return;
        }
    }
    else{
        qDebug()<<"greska";
    }
}



StatisticType Statistics::string_to_stat_type(QString st){
    if(st == "Moving Average")
        return StatisticType::MA;
    else if(st == "EMA")
        return StatisticType::EMA;
    else if(st == "MACD")
        return StatisticType::MACD;
    else return StatisticType::BB;

}


