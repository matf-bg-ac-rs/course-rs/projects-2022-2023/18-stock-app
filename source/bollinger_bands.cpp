#include "bollinger_bands.hpp"
#include "qchart.h"
#include<iostream>
#include "ui_mainwindow.h"
#include <QApplication>
#include <QChart>
#include <QChartView>
#include <QValueAxis>
#include <QAreaSeries>

BollingerBands::BollingerBands(){

}

void BollingerBands::calculateBollingerBands(QCandlestickSeries* candles,int period, int factor){

        QList<QCandlestickSet*>children = candles->sets();

        double total_average = 0;

        for (int i = 0; i < candles->count(); i++)
        {
            QCandlestickSet* child = children.at(i);
            total_average += child->close();

            if (i >= period - 1)
            {
                double total_bollinger = 0;
                double average = total_average / period;

                for (int x = i; x > (i - period); x--)
                {
                    total_bollinger += pow(children.at(x)->close() - average, 2);
                }

                double stdev = sqrt(total_bollinger / period);

                middleLine.emplace_back(average);
                upperBand1.emplace_back(average + factor * stdev);
                lowerBand1.emplace_back(average - factor * stdev);

                total_average -= children.at(i - period + 1)->close();
            }
        }
    }


void BollingerBands::show(QCandlestickSeries* candles,Ui::MainWindow *ui ,QChart* chart){

    calculateBollingerBands(candles,10,2);

    QLineSeries* series = new QLineSeries();

    for (int i = 0; i<(int)middleLine.size();i++){
        series->append(i,middleLine[i]);
    }


    QPen p ;
    p.setWidth(3);
    p.setStyle(Qt::DashLine);
    p.setBrush(QBrush("light blue"));
    series->setPen(p);

    QString title = "Moving average";
    series->setName(title);
    chart->addSeries(series);

    QLineSeries* upperBandSeries = new QLineSeries();
    //std::vector<double> upperBand = this->upperBand(candles,20);

    for (int i = 0; i<(int)upperBand1.size();i++){
        upperBandSeries->append(i,upperBand1[i]);
    }

    QPen p2 ;
    p2.setWidth(2);
    p2.setBrush(QBrush("light green"));
    upperBandSeries->setPen(p2);
    upperBandSeries->setName("Upper Band");

    chart->addSeries(upperBandSeries);

    QLineSeries* lowerBandSeries = new QLineSeries();
    for (int i = 0; i<(int)lowerBand1.size();i++){
        lowerBandSeries->append(i,lowerBand1[i]);
    }

    QPen p3 ;
    p3.setWidth(2);
    p3.setBrush(QBrush("pink"));
    lowerBandSeries->setPen(p3);
    lowerBandSeries->setName("Lower Band");

    chart->addSeries(lowerBandSeries);

    QAreaSeries *areaSeries = new QAreaSeries(upperBandSeries,lowerBandSeries);
    areaSeries->setName(QString("Middle area"));

    areaSeries->setBrush(QColor(255, 255, 0, 0x80));
    chart->addSeries(areaSeries);

    finishShowingStat(ui,chart);


};
