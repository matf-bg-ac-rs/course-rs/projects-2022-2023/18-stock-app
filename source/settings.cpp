#include "settings.hpp"

std::map<Company, QString> Settings::companies = {
    {Company::Amazon, "AMZN"},
    {Company::AMC, "APE"},
    {Company::Apple, "AAPL"},
    {Company::Cisco, "CSCO"},
    {Company::CocaCola, "KO"},
    {Company::Ford, "F"},
    {Company::Intel, "INTC"},
    {Company::KeyCorp, "KEY"},
    {Company::Lyft, "LYFT"},
    {Company::Meta, "META"},
    {Company::Microsoft, "MSFT"},
    {Company::Netflix, "NFLX"},
    {Company::Nokia, "NOK"},
    {Company::Nutanix, "NTNX"},
    {Company::NVIDIA, "NVDA"},
    {Company::Pfizer, "PFE"},
    {Company::Pinterest, "PINS"},
    {Company::Shopify, "SHOP"},
    {Company::Snap, "SNAP"},
    {Company::Tesla, "TSLA"},
    {Company::Uber, "UBER"},
    {Company::WaltDisney, "DIS"},
    {Company::WarnerBros, "WBD"},
};

std::map<Company, QString> Settings::companyToString = {
    {Company::Amazon, "Amazon.com, Inc."},
    {Company::AMC, "AMC Entertainment Holdings, Inc."},
    {Company::Apple, "Apple, Inc."},
    {Company::Cisco, "Cisco Systems, Inc."},
    {Company::CocaCola, "Coca Cola Company"},
    {Company::Ford, "Ford Motor Company"},
    {Company::Intel, "Intel"},
    {Company::KeyCorp, "Key Corp"},
    {Company::Lyft, "Lyft, Inc."},
    {Company::Meta, "Meta Platforms"},
    {Company::Microsoft, "Microsoft Corporation"},
    {Company::Netflix, "Netflix"},
    {Company::Nokia, "Nokia Corp"},
    {Company::Nutanix, "Nutanix, Inc."},
    {Company::NVIDIA, "NVIDIA Corporation"},
    {Company::Pfizer, "Pfizer, Inc."},
    {Company::Pinterest, "Pinterest, Inc."},
    {Company::Shopify, "Shopify"},
    {Company::Snap, "Snap Inc"},
    {Company::Tesla, "Tesla"},
    {Company::Uber, "Uber Technologies Inc"},
    {Company::WaltDisney, "The Walt Disney Company"},
    {Company::WarnerBros, "Warner Bros. Discovery, Inc."},
};

std::map<QString, Company> Settings::stringToCompany = {
    {"Amazon", Company::Amazon},
    {"AMC", Company::AMC},
    {"Apple", Company::Apple},
    {"Cisco Systems, Inc.", Company::Cisco},
    {"Coca Cola Company", Company::CocaCola},
    {"Ford", Company::Ford},
    {"Intel Corporation", Company::Intel},
    {"KeyCorp", Company::KeyCorp},
    {"Lyft", Company::Lyft},
    {"Meta", Company::Meta},
    {"Microsoft Corporation", Company::Microsoft},
    {"Netflix", Company::Netflix},
    {"Nokia Corp", Company::Nokia},
    {"Nutanix, Inc.", Company::Nutanix},
    {"NVIDIA", Company::NVIDIA},
    {"Pfizer, Inc.", Company::Pfizer},
    {"Pinterest", Company::Pinterest},
    {"Shopify Inc.", Company::Shopify},
    {"Snap Inc", Company::Snap},
    {"Tesla", Company::Tesla},
    {"Uber Technologies, Inc.", Company::Uber},
    {"The Walt Disney Company", Company::WaltDisney},
    {"Warner Bros. Discovery, Inc.", Company::WarnerBros},
};

std::vector<std::string> Settings::companies_tickers = {company_ticker_stat(Company::Amazon),
                                                        company_ticker_stat(Company::AMC),
                                                        company_ticker_stat(Company::Apple),
                                                        company_ticker_stat(Company::Cisco),
                                                        company_ticker_stat(Company::CocaCola),
                                                        company_ticker_stat(Company::Ford),
                                                        company_ticker_stat(Company::Intel),
                                                        company_ticker_stat(Company::KeyCorp),
                                                        company_ticker_stat(Company::Lyft),
                                                        company_ticker_stat(Company::Meta),
                                                        company_ticker_stat(Company::Microsoft),
                                                        company_ticker_stat(Company::Netflix),
                                                        company_ticker_stat(Company::Nokia),
                                                        company_ticker_stat(Company::Nutanix),
                                                        company_ticker_stat(Company::NVIDIA),
                                                        company_ticker_stat(Company::Pfizer),
                                                        company_ticker_stat(Company::Pinterest),
                                                        company_ticker_stat(Company::Shopify),
                                                        company_ticker_stat(Company::Snap),
                                                        company_ticker_stat(Company::Tesla),
                                                        company_ticker_stat(Company::Uber),
                                                        company_ticker_stat(Company::WaltDisney),
                                                        company_ticker_stat(Company::WarnerBros)};


Settings::Settings(Company c, time_t time_f, time_t to, Interval i):
 company(c), time_from(time_f), time_to(to), i(i)
{
}

std::string Settings::company_to_string() const{

    return companyToString[this->company].toStdString();
}



std::string Settings::get_interval_str() const
{
    switch(i){

    case Interval::m_1:
        return "1m";
    case Interval::m_5:
        return "5m";
    case Interval::h_1:
        return "1h";
    case Interval::d_1:
        return "1d";
    case Interval::mo_1:
        return "1mo";
    case Interval::mo_3:
        return "3mo";
    case Interval::y_1:
        return "1y";
    default:
        return "Error with interval";
    }
}

time_t Settings::get_time_from() const{
    return time_from;
}
time_t Settings::get_time_to() const{
    return time_to;
}


Company Settings::string_to_company(QString s_company){

    return stringToCompany[s_company];
}

std::string Settings::company_ticker() const{


    return companies[this->company].toStdString();
}

std::string Settings::company_ticker_stat(Company c)
{
    return companies[c].toStdString();
}

Interval Settings::string_to_interval(QString s_interval)
{
    if(s_interval == "1 minute"){
        return Interval::m_1;
    }
    else if(s_interval == "5 minutes"){
        return Interval::m_5;
    }
    else if(s_interval == "1 hour"){
        return Interval::h_1;
    }
    else if(s_interval == "1 day"){
        return Interval::d_1;
    }
    else if(s_interval == "1 month"){
        return Interval::mo_1;
    }
    else if(s_interval == "3 months"){
        return Interval::mo_3;
    }
    else{
        return Interval::y_1;
    }
}
