#ifndef CHART_H
#define CHART_H

#include <QChart>
#include <QGestureEvent>


QT_BEGIN_NAMESPACE
class QGestureEvent;
QT_END_NAMESPACE

QT_USE_NAMESPACE

class Chart : public QChart
{
public:
    explicit Chart(QGraphicsItem *parent = nullptr, Qt::WindowFlags wFlags = {});
    ~Chart();

protected:
    bool sceneEvent(QEvent *event);

private:
    bool gestureEvent(QGestureEvent *event);
};

#endif // CHART_H
