#include "parsernyse.hpp"
#include <iostream>
#include <string>

ParserNYSE::ParserNYSE()
{

}

bool find_string(const std::string& s, std::string& key, std::regex& rx){
    std::smatch m;
    std::regex_search(s,m,rx);

    for(auto match: m){
        key = match;
        return true;
    }

    return false;
}

std::vector<int> filter_integers(const std::string &s){
    size_t first = s.find('[');
    size_t last = s.find(']');

    //takes only numbers without [ and ]
    std::string s_numbers= s.substr(first+1, last - first - 1);

    //spliting string by ,
    std::vector<int> ret_val;

    //creates string stream from string s_numbers
    std::stringstream X(s_numbers);

    std::string x;
    while(getline(X, x, ',')){
        int value;
        try {
            value = std::stoi(x);
        } catch (std::invalid_argument& e) {
            continue;
        }
        ret_val.emplace_back(value);
    }

    return ret_val;
}

std::vector<double> filter_doubles(const std::string &s){
    size_t first = s.find('[');
    size_t last = s.find(']');

    std::string s_numbers=s.substr(first+1, last - first - 1);

    std::vector<double> ret_val;
    std::stringstream X(s_numbers);

    std::string x;
    while(getline(X,x,',')){
        double value;
        try {
            value = std::stod(x);
        } catch (std::invalid_argument& e) {
            continue;
        }
        ret_val.emplace_back(value);
    }

    return ret_val;
}


QPair<QString, QCandlestickSeries*> ParserNYSE::parse(const std::string &s) const
{
    std::regex rxt("\"timestamp\":[\\[|\\]|0-9|,|.]*");
    std::regex rxo("\"open\":[\\[|\\]|0-9|,|.|null]*");
    std::regex rxh("\"high\":[\\[|\\]|0-9|,|.|null]*");
    std::regex rxc("\"close\":[\\[|\\]|0-9|,|.|null]*");
    std::regex rxl("\"low\":[\\[|\\]|0-9|,|.|null]*");

    std::string s_timestamp;
    std::string s_open;
    std::string s_high;
    std::string s_close;
    std::string s_low;


    std::string err;
    std::string err_desc;
    std::string msg;
    std::string quote;

    std::regex rxerr("\"error\":.*");
    std::regex rxerr_desc("\"description\":.*");
    std::regex rxmsg("[0-9|A-Z].*");
    std::regex rxquote("\"quote\":\\[\\{\\}\\]");


    find_string(s, err, rxerr);
    find_string(s, err_desc, rxerr_desc);
    find_string(err_desc, msg, rxmsg);
    find_string(s, quote, rxquote);

    if (err.find("null") == std::string::npos) {
        return qMakePair(QString::fromStdString(msg), nullptr);
    }

    if(quote.find("\"quote\":[{}]") != std::string::npos){
        return qMakePair(QString::fromStdString("No data for showing!"), nullptr);
    }

    if(!find_string(s, s_timestamp, rxt)) {
        return qMakePair(QString::fromStdString("No data for showing!"), nullptr);
    }

    if(!find_string(s, s_open, rxo)) {
        return qMakePair(QString::fromStdString("No data for showing!"), nullptr);
    }

    if(!find_string(s, s_high, rxh)) {
        return qMakePair(QString::fromStdString("No data for showing!"), nullptr);
    }

    if(!find_string(s, s_close, rxc)) {
        return qMakePair(QString::fromStdString("No data for showing!"), nullptr);
    }

    if(!find_string(s, s_low, rxl)) {
        return qMakePair(QString::fromStdString("No data for showing!"), nullptr);
    }

    const std::vector<int> v_timestamp = filter_integers(s_timestamp);
    const std::vector<double> v_open = filter_doubles(s_open);
    const std::vector<double> v_high = filter_doubles(s_high);
    const std::vector<double> v_close = filter_doubles(s_close);
    const std::vector<double> v_low = filter_doubles(s_low);
    const size_t n = v_open.size();

    QCandlestickSeries* candles = new QCandlestickSeries();
    candles->setName("Ntflx Ltd");



    for(size_t i = 0; i < n-1; i++){
        QCandlestickSet* candle = new QCandlestickSet(qint64(v_timestamp[i]),candles);
        candle->setOpen(qreal(v_open[i]));
        candle->setHigh(qreal(v_high[i]));
        candle->setClose(qreal(v_close[i]));
        candle->setLow(qreal(v_low[i]));
        candles->append(candle);
    }

    return qMakePair(QString(), candles);
}

double ParserNYSE::parseTrending(std::string &s) const
{

    std::regex rxv("\"volume\":[\\[|\\]|0-9|,|.]*");

    std::string s_volume;


    if(!find_string(s, s_volume, rxv)) {
        std::cerr<<"Can't find volume in string\n";
        exit(EXIT_FAILURE);
    }

    std::vector<int> v_volume = filter_integers(s_volume);

    return std::accumulate(v_volume.cbegin(), v_volume.cend(), 0) / static_cast<double>(v_volume.size());
}
