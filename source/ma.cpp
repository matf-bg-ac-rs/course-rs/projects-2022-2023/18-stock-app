#include "ma.hpp"
#include "qchart.h"
#include<iostream>
#include "ui_mainwindow.h"

#include <QApplication>
#include <QChart>
#include <QChartView>
#include <QValueAxis>

MA::MA(){

}

void MA::calculate(QCandlestickSeries* candles,int n){


    QList<QCandlestickSet*>children = candles->sets();

    double sum = 0;

    for (int i = 0;i < children.count();i++){
        QCandlestickSet* child = children.at(i);

        sum += child->close();
        if (i>=n-1){
            double indicator = sum/n;

            indicators.emplace_back(indicator);
            sum -= children.at(i-n+1)->close();
        }
    }


}

void MA::show(QCandlestickSeries *candles, Ui::MainWindow *ui , QChart* chart){

    calculate(candles,20);

    QLineSeries* series = new QLineSeries();

    for (int i = 0; i<(int)indicators.size();i++){
        series->append(i,indicators[i]);
    }

    QString title = "Moving Average";

    QPen p ;
    p.setWidth(3);
    p.setBrush(QBrush("light blue"));
    series->setPen(p);

    series->setName(title);

    chart->addSeries(series);

    finishShowingStat(ui,chart);

};
