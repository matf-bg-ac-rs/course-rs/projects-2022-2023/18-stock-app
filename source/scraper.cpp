#define CURL_STATICLIB
#include "scraper.hpp"
#include<string.h>
#include <iostream>

Scraper::Scraper(std::string& url, Settings setting, ParserNYSE *parser, QObject *parent):
    url(url), settings(setting), parser(parser), parent(parent)
{

}

void Scraper::replace(std::string& url, const std::string& placeholder, const std::string& replacement){
    size_t index = 0;
    index = url.find(placeholder, index);
    size_t placeholder_len = placeholder.length();
    url.replace(index, placeholder_len, replacement);
}

void Scraper::replace_placeholders(std::string& url, const Settings& settings){

    std::string ticker = "<ticker>";
    std::string from = "<from>";
    std::string to = "<to>";
    std::string i = "<i>";

    replace(url, ticker, settings.company_ticker());
    replace(url,from, std::to_string(settings.get_time_from()));
    replace(url, to, std::to_string(settings.get_time_to()));
    replace(url, i, settings.get_interval_str());

}


size_t Scraper::write_function(void* ptr, size_t size, size_t nmemb, std::string* data) {
    data->append((char*)ptr, size * nmemb);
    return size * nmemb;
}


QPair<QString, QCandlestickSeries *> Scraper::get_content(std::string& url, const Settings& settings, const ParserNYSE* parser){
    Scraper::replace_placeholders(url, settings);
    std::string response_string;
    std::string header_string;
    auto curl = curl_easy_init();
    if(curl){
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());


        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, Scraper::write_function);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_string);
        curl_easy_setopt(curl, CURLOPT_HEADERDATA, &header_string);

        auto res = curl_easy_perform(curl);
        if (res != 0){
            std::cerr << "Something is wrong with scraper" << std::endl;
            std::exit(EXIT_FAILURE);
        }

       curl_easy_cleanup(curl);
    }

    curl_global_cleanup();

    return parser->parse(response_string);
}

void Scraper::run()
{
    auto [err, candles] = Scraper::get_content(url, settings, parser);
    delete parser;
    if(!err.isNull()) {
        emit Scraper::error_found(err);
        return;
    }

    if(candles->count() == 0) {
        emit Scraper::error_found(tr("No data for showing"));
        return;
    }
    // moving object to main thread
    candles->moveToThread(parent->thread());
    emit Scraper::data_is_ready(candles, settings);
}

