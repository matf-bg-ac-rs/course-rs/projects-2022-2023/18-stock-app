#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "scraper.hpp"
#include <string>
#include <iostream>

#include <QApplication>
#include <QBarCategoryAxis>
#include <QChart>
#include <QChartView>
#include <QValueAxis>
#include <QMessageBox>
#include <QStringListModel>
#include "statistics.hpp"
#include "trendingscraper.hpp"



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->cbCompany->addItem("Amazon");
    ui->cbCompany->addItem("AMC");
    ui->cbCompany->addItem("Apple");;
    ui->cbCompany->addItem("Cisco Systems, Inc.");
    ui->cbCompany->addItem("Coca Cola Company");
    ui->cbCompany->addItem("Ford");
    ui->cbCompany->addItem("Intel Corporation");
    ui->cbCompany->addItem("KeyCorp");
    ui->cbCompany->addItem("Lyft");
    ui->cbCompany->addItem("Meta");
    ui->cbCompany->addItem("Microsoft Corporation");
    ui->cbCompany->addItem("NVIDIA");
    ui->cbCompany->addItem("Netflix");
    ui->cbCompany->addItem("Nokia Corp");
    ui->cbCompany->addItem("Pfizer, Inc.");
    ui->cbCompany->addItem("Pinterest");
    ui->cbCompany->addItem("Shopify Inc.");
    ui->cbCompany->addItem("Snap Inc.");
    ui->cbCompany->addItem("Tesla");
    ui->cbCompany->addItem("Uber Technologies, Inc.");
    ui->cbCompany->addItem("The Walt Disney Company");
    ui->cbCompany->addItem("Warner Bros. Discovery, Inc.");

    QDate date = QDate::currentDate();
    ui->deFrom->setDate(date.addDays(-1));
    ui->deTo->setDate(date);
    ui->deFrom->setMinimumDate(QDate::currentDate().addYears(-5));
    ui->deTo->setMinimumDate(QDate::currentDate().addYears(-5));

    ui->leClose->setEnabled(false);
    ui->leHigh->setEnabled(false);
    ui->leLow->setEnabled(false);
    ui->leOpen->setEnabled(false);
    ui->leTimestamp->setEnabled(false);

}

MainWindow::~MainWindow()
{
    delete ui;
}

Settings MainWindow::read_settings() const {
    // gettig time
    const QDateTime time_from = ui->deFrom->dateTime();
    const time_t from = time_from.toSecsSinceEpoch();

    const QDateTime time_to = ui->deTo->dateTime();
    const time_t to = time_to.toSecsSinceEpoch();

//    getting company
    QString c = ui->cbCompany->currentText();

//    getting time interval
    const QString interval = ui->cbInterval->currentText();

    return Settings(Settings::string_to_company(c), from, to, Settings::string_to_interval(interval));
}

Settings MainWindow::get_init_settings() const{

    QDateTime now = QDateTime::currentDateTime();
    QDateTime month_ago = now.addMonths(-1);
    time_t from = month_ago.toSecsSinceEpoch();
    time_t to = now.toSecsSinceEpoch();

    std::string url = "https://query1.finance.yahoo.com/v8/finance/chart/<ticker>?period1=<from>&period2=<to>&interval=<i>";
    const Settings s(Company::Netflix, from, to, Interval::d_1);

    return s;
}

// only on app startup
void MainWindow::init_data()
{
    const Settings s = get_init_settings();
    std::string url = "https://query1.finance.yahoo.com/v8/finance/chart/<ticker>?period1=<from>&period2=<to>&interval=<i>";

    Scraper *scraper = new Scraper(url,s, new ParserNYSE(), this);
    TrendingScraper *trending_scraper = new TrendingScraper(url, s, new ParserNYSE(), this);
    connect(scraper, &Scraper::data_is_ready, this, &MainWindow::draw_chart);
    connect(scraper, &Scraper::error_found, this, &MainWindow::show_message_box);
    connect(scraper, &Scraper::finished, scraper, &Scraper::deleteLater);
    scraper->start();

    connect(trending_scraper, &TrendingScraper::trending_ready, this, &MainWindow::show_trending);
    connect(trending_scraper, &TrendingScraper::data_is_ready_splash,this, &MainWindow::splash_should_close_slot);
    connect(trending_scraper, &TrendingScraper::finished, trending_scraper, &TrendingScraper::deleteLater);
    trending_scraper->start();

}

void MainWindow::on_pbShowCandlestickChart_clicked()
{
    const Settings s = read_settings();

    if(s.get_time_from() >= s.get_time_to()) {
        show_message_box(tr("Invalid dates"));
        return;
    }

    std::string url = "https://query1.finance.yahoo.com/v8/finance/chart/<ticker>?period1=<from>&period2=<to>&interval=<i>";

    Scraper *thread = new Scraper(url, s, new ParserNYSE(), this);

    connect(thread, &Scraper::data_is_ready, this, &MainWindow::draw_chart);
    connect(thread, &Scraper::error_found, this, &MainWindow::show_message_box);
    connect(thread, &Scraper::finished, thread, &Scraper::deleteLater);
    thread->start();
}

void MainWindow::draw_chart(QCandlestickSeries *candles, const Settings& s)
{
    QString name = QString::fromStdString(s.company_to_string());
    candles->setName(name);
    candles->setIncreasingColor(QColor(Qt::green));
    candles->setDecreasingColor(QColor(Qt::red));

    QStringList timestamps;
    for(const auto &set: candles->sets()){
        timestamps << QDateTime::fromSecsSinceEpoch(set->timestamp()).toString("dd/MM/yy hh:mm");
    }
    auto n = timestamps.length();
    auto first_value = candles->sets().at(0)->open();
    auto last_value = candles->sets().at(n-1)->close();

    auto period_rate = 100*(last_value - first_value)/first_value;

    QChart *chart = new QChart();
    chart->addSeries(candles);
    QString title = name.append(" Historical Data " + QString::number(period_rate) + "%");
    chart->setTitle(title);
    chart->setAnimationOptions(QChart::SeriesAnimations);

    chart->createDefaultAxes();

//    we set custom categories for the horizontal axis by querying the pointer for the axis from the chart
    QBarCategoryAxis *axisX = qobject_cast<QBarCategoryAxis *>(chart->axes(Qt::Horizontal).at(0));
    axisX->setCategories(timestamps);

    QValueAxis *axisY = qobject_cast<QValueAxis *>(chart->axes(Qt::Vertical).at(0));
    axisY->setMax(axisY->max() * 1.01);
    axisY->setMin(axisY->min() * 0.99);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

//    we add the chart onto a view
    ui->candlestickChart->setChart(chart);
    ui->candlestickChart->setRenderHint(QPainter::Antialiasing);


    connect(candles, &QCandlestickSeries::hovered, this, &MainWindow::display_candle_values);

    emit candles_shown(candles,ui,chart);

}

void MainWindow::show_trending(QList<std::pair<std::string, double>>* trending_companies){

    QStringListModel* model = new QStringListModel(this);
    QStringList list;
    size_t n  = 15;
    for(unsigned int i = 0;  i < n; i++){
        list << QString::number(i+1) + QString(". ") + QString::fromStdString(trending_companies->at(i).first) + QString(" (vol: ") + QString::number(static_cast<int>(trending_companies->at(i).second)) + QString(")");
    }
    model->setStringList(list);
    ui->lvTrendingStocks->setModel(model);
    delete trending_companies;
}

void MainWindow::refresh_trending(QList<std::pair<std::string, double>>* trending_companies){

    ui->lvTrendingStocks->model()->removeRows(0, ui->lvTrendingStocks->model()->rowCount());
    delete ui->lvTrendingStocks->model();
    QStringListModel* model = new QStringListModel(ui->lvTrendingStocks->model());
    QStringList list;
    size_t n  = 15;
    for(unsigned int i = 0;  i < n; i++){
        list << QString::number(i+1) + QString(". ") + QString::fromStdString(trending_companies->at(i).first) + QString(" (vol: ") + QString::number(static_cast<int>(trending_companies->at(i).second)) + QString(")");
    }
    model->setStringList(list);
    ui->lvTrendingStocks->setModel(model);
    delete trending_companies;
}

void MainWindow::splash_should_close_slot()
{
    emit MainWindow::spalsh_should_close();
}

void MainWindow::on_pbShowStatistic_clicked()
{
    const Settings s = read_settings();

    if(s.get_time_from() >= s.get_time_to()) {
        show_message_box(tr("Invalid dates"));
        return;
    }


    QString statisticType = ui->cbStatistics->currentText();
    Statistics* stat = new Statistics(Statistics::string_to_stat_type(statisticType));
    std::string url = "https://query1.finance.yahoo.com/v8/finance/chart/<ticker>?period1=<from>&period2=<to>&interval=<i>";

    Scraper *thread = new Scraper(url, s, new ParserNYSE(), this);

    connect(thread, &Scraper::data_is_ready, this, &MainWindow::draw_chart);
    connect(this,&MainWindow::candles_shown,stat,&Statistics::startDrawingStats);
    connect(thread, &Scraper::finished, thread, &Scraper::deleteLater);

    thread->start();

}


void MainWindow::on_pbRefresh_clicked(){

    QDateTime now = QDateTime::currentDateTime();
    QDateTime month_ago = now.addMonths(-1);
    time_t from = month_ago.toSecsSinceEpoch();
    time_t to = now.toSecsSinceEpoch();

    std::string url = "https://query1.finance.yahoo.com/v8/finance/chart/<ticker>?period1=<from>&period2=<to>&interval=<i>";
    const Settings s(Company::Netflix, from, to, Interval::d_1);

    TrendingScraper *trending_scraper = new TrendingScraper(url, s, new ParserNYSE(), this);

    connect(trending_scraper, &TrendingScraper::refresh_ready, this, &MainWindow::refresh_trending);
    trending_scraper->start();
}


void MainWindow::show_message_box(QString error) {
    QMessageBox box;
    QString title = "Error";
    box.warning(QApplication::activeWindow(), title, error);
}

void MainWindow::display_candle_values(bool status, QCandlestickSet *set)
{
    if (status == true){
        QDateTime date = QDateTime::fromSecsSinceEpoch(set->timestamp());
        ui->leTimestamp->setText(date.toString());
        ui->leClose->setText(QString::number(set->close()));
        ui->leOpen->setText(QString::number(set->open()));
        ui->leLow->setText(QString::number(set->low()));
        ui->leHigh->setText(QString::number(set->high()));
    }

}
