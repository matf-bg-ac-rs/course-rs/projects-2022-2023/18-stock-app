#ifndef MA_HPP
#define MA_HPP

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QCandlestickSet>
#include <QCandlestickSeries>
#include <QLineSeries>
#include<QObjectList>
#include<deque>
#include<vector>
#include"statistics_adapter.hpp"

class MA : public StatisticsAdapter{
private:
    std::vector<double> indicators;

public:

    MA();

    void calculate(QCandlestickSeries* candles,int n);

    virtual void show(QCandlestickSeries* candles,Ui::MainWindow *ui,QChart* chart) override;
};

#endif // MA_HPP
