#ifndef TRENDINGSCRAPER_HPP
#define TRENDINGSCRAPER_HPP

#include <QThread>
#include <QDate>

#include <curl/curl.h>
#include "scraper.hpp"
#include"settings.hpp"
#include "parsernyse.hpp"

class TrendingScraper : public Scraper
{
    Q_OBJECT
public:
    TrendingScraper(std::string& url, Settings settings, ParserNYSE *parser, QObject *parent);

    static double get_content(std::string& url, Settings settings, ParserNYSE* parser);
    static QList<std::pair<std::string, double>>* find_trending_companies(std::string &&interval);

private:

    static void replace_placeholders(std::string& url, const Settings& settings);
protected:

    void run();

signals:
    void trending_ready(QList<std::pair<std::string, double>>* trending_companies);
    void refresh_ready(QList<std::pair<std::string, double>>* trending_companies);
    void data_is_ready_splash();

};

#endif // TRENDINGSCRAPER_HPP
