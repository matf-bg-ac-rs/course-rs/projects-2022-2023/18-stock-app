#ifndef STATISTICS_ADAPTER_H
#define STATISTICS_ADAPTER_H

#include "qcandlestickseries.h"
#include "qobject.h"
#include "ui_mainwindow.h"
class StatisticsAdapter : public QObject{

protected:
    void finishShowingStat(Ui::MainWindow *ui ,QChart* chart);

public:
   StatisticsAdapter();

   virtual void show(QCandlestickSeries *, Ui::MainWindow *, QChart* );

};

#endif // STATISTICS_ADAPTER_H
