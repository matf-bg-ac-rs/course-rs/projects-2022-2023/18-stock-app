#include "mainwindow.h"
#include <QApplication>
#include <QTimer>
#include <QSplashScreen>
#include <iostream>
#include <QFile>
#include <QEventLoop>
#include <QObject>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QPixmap pixmap(":/images/fluctuation.png");

    QFile styleSheetFile(":/qss/Fibers.qss");
    styleSheetFile.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(styleSheetFile.readAll());
    a.setStyleSheet(styleSheet);

    MainWindow w;
    QSplashScreen splash(pixmap);
//  prevents the user from closing the splash screen by clicking on it
    splash.setEnabled(false);

    splash.show();
    a.processEvents();

    QEventLoop loop;
    QTimer::singleShot(0, &w, &MainWindow::init_data);
    // exit the loop when this signal is emmited so that splash screen last as long as it takes for data to be initialized
    QObject::connect(&w, SIGNAL(spalsh_should_close()),&loop, SLOT(quit()));
    loop.exec();

    w.show();
    splash.finish(&w);

    return a.exec();
}
