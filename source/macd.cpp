#include "macd.hpp"
#include<iostream>
#include<vector>
#include"ema.hpp"
#include "ui_mainwindow.h"


MACD::MACD()
{

}

void MACD::calculateMACD(QCandlestickSeries* candles) {

    //EMA* newEMA = new EMA;

    ema_26th = EMA::calculate(candles,26);
    ema_12th = EMA::calculate(candles,12);

    signal_line = EMA::calculate(candles,9);

}

void MACD::show(QCandlestickSeries* candles,Ui::MainWindow *ui ,QChart* chart){

    calculateMACD(candles);

    QLineSeries* signal_line_series = new QLineSeries();

    for (int i = 0; i<(int)signal_line.size();i++){
        signal_line_series->append(i,signal_line[i]);
    }

    QPen p1 ;
    p1.setWidth(2);
    p1.setStyle(Qt::DashLine);
    p1.setBrush(QBrush("light blue"));
    signal_line_series->setPen(p1);
    signal_line_series->setName(QString("Signal line"));

    chart->addSeries(signal_line_series);

    QLineSeries* ema_26th_series = new QLineSeries();

    for (int i = 0; i<(int)ema_26th.size();i++){
        ema_26th_series->append(i,ema_26th[i]);
    }

    QPen p2 ;
    p2.setWidth(2);
    p2.setBrush(QBrush("orange"));
    ema_26th_series->setPen(p2);
    ema_26th_series->setName(QString("26th EMA"));

    chart->addSeries(ema_26th_series); // signal line

    QLineSeries* ema_12th_series = new QLineSeries();

    for (int i = 0; i<(int)ema_12th.size();i++){
        ema_12th_series->append(i,ema_12th[i]);
    }

    QPen p3 ;
    p3.setWidth(2);
    p3.setBrush(QBrush("purple"));
    ema_12th_series->setPen(p3);
    ema_12th_series->setName(QString("12th EMA"));

    chart->addSeries(ema_12th_series);


    finishShowingStat(ui,chart);

};






