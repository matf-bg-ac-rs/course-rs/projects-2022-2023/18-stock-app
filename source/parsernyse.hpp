#ifndef PARSERNYSE_HPP
#define PARSERNYSE_HPP

#include <string>
#include <regex>
#include <vector>
#include <QCandlestickSet>
#include <QCandlestickSeries>

class ParserNYSE
{
public:
    ParserNYSE();

    QPair<QString,QCandlestickSeries*> parse(const std::string &scraped_string) const;
    double parseTrending(std::string &s) const;    
};

bool find_string(const std::string& s, std::string& key, std::regex& rx);
std::vector<int> filter_integers(const std::string &s);
std::vector<double> filter_doubles(const std::string &s);

#endif // PARSERNYSE_HPP
