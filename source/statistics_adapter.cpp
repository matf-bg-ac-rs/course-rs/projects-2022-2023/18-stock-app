#include "statistics_adapter.hpp"
#include "ui_mainwindow.h"

StatisticsAdapter::StatisticsAdapter(){}


void StatisticsAdapter::finishShowingStat(Ui::MainWindow *ui ,QChart* chart){
    chart->setAnimationOptions(QChart::SeriesAnimations);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    //    we add the chart onto a view
    ui->candlestickChart->setChart(chart);
    ui->candlestickChart->setRenderHint(QPainter::Antialiasing);
}


void StatisticsAdapter::show(QCandlestickSeries *, Ui::MainWindow *, QChart* ){

}






