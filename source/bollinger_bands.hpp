#ifndef BOLLINGER_BANDS_HPP
#define BOLLINGER_BANDS_HPP

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QCandlestickSet>
#include <QCandlestickSeries>
#include <QLineSeries>
#include<QObjectList>
#include<deque>
#include<vector>
#include "statistics_adapter.hpp"

class BollingerBands : public StatisticsAdapter{

private:
    std::vector<double>upperBand1;
    std::vector<double>lowerBand1;
    std::vector<double>middleLine;

public:

    BollingerBands();

    void calculateBollingerBands(QCandlestickSeries* candles,int n,int factor);

    virtual void show(QCandlestickSeries *candles, Ui::MainWindow *ui, QChart* chart) override;


};

#endif // BOLLINGER_BANDS_HPP
