
#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <QString>
#include <ctime>
#include <string>
#include<map>

enum class Company{
    AMC,
    Apple,
    Amazon,
    Cisco,
    CocaCola,
    Ford,
    Intel,
    KeyCorp,
    Lyft,
    Meta,
    Microsoft,
    Netflix,
    Nokia,
    Nutanix,
    NVIDIA,
    Pfizer,
    Pinterest,
    Snap,
    Shopify,
    Tesla,
    Uber,
    WaltDisney,
    WarnerBros
};

enum class Interval{
    m_1,
    m_5,
    h_1,
    d_1,
    mo_1,
    mo_3,
    y_1
};

class Settings
{

private:
    Company company;
    time_t time_from;
    time_t time_to;
    Interval i;
public:

    Settings(Company company, time_t time_from, time_t time_to, Interval i);

    std::string company_to_string() const;

    std::string company_ticker() const;
    std::string get_interval_str() const;

    time_t get_time_from() const;
    time_t get_time_to() const;

    static std::map<Company, QString> companies;
    static std::map<Company, QString> companyToString;
    static std::map<QString, Company> stringToCompany;

    static std::vector<std::string> companies_tickers;
    static Company string_to_company(QString s_company);
    static std::string company_ticker_stat(Company c);
    static Interval string_to_interval(QString s_interval);
};



#endif // SETTINGS_HPP
