#ifndef STATISTICS_HPP
#define STATISTICS_HPP

#include <QCandlestickSeries>
#include <QLineSeries>
#include "ui_mainwindow.h"

enum class StatisticType{
    MA, //Moving Average
    EMA, //Exponential moving average
    MACD, //Moving average convergence/divergence
    BB// Bollinger Bands
};


class Statistics : public QObject{

private:
    StatisticType statType;
    bool isMAShown=false;
    bool isEMAShown=false;
    bool isMACDShown=false;
    bool isBBShown=false;
    bool isThereEnoughData(QCandlestickSeries *candles);

public:
   Statistics();
   Statistics(StatisticType type);

   static StatisticType string_to_stat_type(QString st);

public slots:
   void startDrawingStats(QCandlestickSeries *s, Ui::MainWindow *ui, QChart* chart);

};

#endif // STATISTICS_HPP
