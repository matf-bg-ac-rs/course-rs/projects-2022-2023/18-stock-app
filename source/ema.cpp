#include "ema.hpp"
#include "ma.hpp"
#include "qchart.h"
#include<iostream>
#include "ui_mainwindow.h"
#include <QApplication>
#include <QChart>
#include <QChartView>
#include <QValueAxis>

EMA::EMA(){

    movAvg = new MA();

}

double EMA::EMAn(QCandlestickSeries* candles,int from,int to){

    QList<QCandlestickSet*>children = candles->sets();

    double num = to - from +1;

    if(num == 1)
        return children.at(from)->close();

    double k = 2/(double)(num+1);

    return (double) children.at(to)->close() * k + (double)EMAn(candles,from,to-1)*(1-k);
}

std::vector<double> EMA::calculate(QCandlestickSeries* candles,int n){

    std::vector<double> indicators = std::vector<double>();

    for (int i=0;i<(int)candles->count()-n+1;i++){
        indicators.emplace_back(EMAn(candles,i,i+n-1));
    }

    return indicators;

}

void EMA::show(QCandlestickSeries* candles,Ui::MainWindow *ui ,QChart* chart){

    indicators = calculate(candles,20);

    QLineSeries* series = new QLineSeries();

    //int len = candles->count();

    for (int i = 0; i<(int)indicators.size();i++){
        series->append(i,indicators[i]);
    }

    QPen p ;
    p.setWidth(3);
    p.setBrush(QBrush("orange"));
    series->setPen(p);
    QString title = "Exponential Moving Average";
    series->setName(title);

    chart->addSeries(series);

    finishShowingStat(ui,chart);
};
