#ifndef MACD_HPP
#define MACD_HPP

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QCandlestickSet>
#include <QCandlestickSeries>
#include <QLineSeries>
#include<QObjectList>
#include <deque>
#include <ema.hpp>

class MACD : public EMA
{

private:
    std::vector<double> ema_26th;
    std::vector<double> ema_12th;
    std::vector<double> signal_line;

public:
    MACD();

    void calculateMACD(QCandlestickSeries* candles);
    virtual void show(QCandlestickSeries* candles,Ui::MainWindow *ui ,QChart* chart) override;


};

#endif // MACD_HPP
